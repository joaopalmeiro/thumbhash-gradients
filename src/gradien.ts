import { globStream } from 'glob';
import path from 'node:path';
import sharp from 'sharp';
import { rgbaToThumbHash, thumbHashToRGBA } from 'thumbhash';

import { INPUT_PATTERN } from './constants';
import { ensureOutputFolder } from './utils';

interface Metadatum {
  outputWidth: number;
  outputHeight: number;
  scaleFactor: number;
  addNoise: boolean;
}

const METADATA: Metadatum[] = [
  // For https://codeberg.org/joaopalmeiro/mobile-photos:
  {
    outputWidth: 2400,
    outputHeight: 3200,
    scaleFactor: 1,
    addNoise: false,
  },
  // For MacBook Air (M1, 2020):
  {
    outputWidth: 2560,
    outputHeight: 1600,
    scaleFactor: 2,
    addNoise: true,
  },
  {
    outputWidth: 2560,
    outputHeight: 1600,
    scaleFactor: 2,
    addNoise: false,
  },
  // For Xiaomi Redmi Note 9:
  {
    outputWidth: 1080,
    outputHeight: 2340,
    scaleFactor: 2,
    addNoise: true,
  },
  {
    outputWidth: 1080,
    outputHeight: 2340,
    scaleFactor: 2,
    addNoise: false,
  },
  // For https://codeberg.org/joaopalmeiro/generative-art/src/branch/main/strange-attractors:
  {
    // outputWidth: 1600,
    // outputHeight: 1600,
    outputWidth: 1080,
    outputHeight: 1080,
    scaleFactor: 1,
    addNoise: false,
  },
];

const main = async (): Promise<void> => {
  // console.log(sharp.versions);
  console.log(`Concurrency: ${sharp.concurrency()}`);
  // sharp.cache(false);

  for (const { outputWidth, outputHeight, scaleFactor, addNoise } of METADATA) {
    for await (const inputPath of globStream(INPUT_PATTERN, { withFileTypes: false })) {
      const outputFolder = `output/${outputWidth}x${outputHeight}`;
      ensureOutputFolder(outputFolder);

      const { data: inputImg, info } = await sharp(inputPath)
        .raw()
        .ensureAlpha()
        .resize({
          width: 100,
          height: 100,
          // fit: 'inside',
          fit: 'cover',
        })
        .toBuffer({ resolveWithObject: true });

      const hash = rgbaToThumbHash(info.width, info.height, inputImg);
      const { w: rgbaWidth, h: rgbaHeight, rgba } = thumbHashToRGBA(hash);

      const outputImg = sharp(rgba, {
        raw: {
          width: rgbaWidth,
          height: rgbaHeight,
          channels: info.channels,
        },
      }).resize({
        width: outputWidth * scaleFactor,
        height: outputHeight * scaleFactor,
        fit: 'fill',
        // fit: 'cover',
        kernel: 'lanczos3',
      });

      const outputPrefix = addNoise ? 'noise-' : '';
      const outputPath = `${outputFolder}/${outputPrefix}${path.parse(inputPath).name}.png`;

      if (addNoise) {
        const noiseImg = await sharp({
          create: {
            width: outputWidth * scaleFactor,
            height: outputHeight * scaleFactor,
            channels: 3,
            background: { r: 0, g: 0, b: 0 },
            noise: {
              type: 'gaussian',
              mean: 128,
              sigma: 30,
            },
          },
        })
          .ensureAlpha(0.3)
          .greyscale()
          .png()
          .toBuffer();

        await outputImg.composite([{ input: noiseImg }]).toFile(outputPath);
      } else {
        await outputImg.toFile(outputPath);
      }

      console.log(`${inputPath} → ${outputPath} ✔`);
    }
  }
  console.log('Done!');
};

main();
