import { ensureDir } from 'fs-extra';

export const ensureOutputFolder = async (outputFolder: string): Promise<void> => {
  try {
    await ensureDir(outputFolder);
  } catch (err) {
    console.error(err);
  }
};
