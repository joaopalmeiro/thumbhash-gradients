import { globStream } from 'glob';
import path from 'node:path';
import type { FitEnum, Sharp } from 'sharp';
import sharp from 'sharp';
import { rgbaToThumbHash, thumbHashToRGBA } from 'thumbhash';

import { INPUT_PATTERN } from './constants';
import { ensureOutputFolder } from './utils';

// const OUTPUT_WIDTH: number = 2400;
// const OUTPUT_HEIGHT: number = 3200;

const BASE_SCALE_FACTOR: number = 5
const OUTPUT_SCALE_FACTOR: number = BASE_SCALE_FACTOR + 1

const BASE_WIDTH: number = 1080
const BASE_HEIGHT: number = 1350
const OUTPUT_WIDTH: number = BASE_WIDTH * OUTPUT_SCALE_FACTOR
const OUTPUT_HEIGHT: number = BASE_HEIGHT * OUTPUT_SCALE_FACTOR

const OUTPUT_FOLDER: string = `output/v2-${OUTPUT_WIDTH}x${OUTPUT_HEIGHT}`;

const FIT_TYPES: (keyof FitEnum)[] = ['inside', 'cover'];

const generateGradient = async (inputPath: string, inputFit: keyof FitEnum): Promise<Sharp> => {
  const { data: inputImg, info } = await sharp(inputPath)
    .raw()
    .ensureAlpha()
    .resize({
      width: 100,
      height: 100,
      fit: inputFit,
    })
    .toBuffer({ resolveWithObject: true });

  const hash = rgbaToThumbHash(info.width, info.height, inputImg);
  const { w: rgbaWidth, h: rgbaHeight, rgba } = thumbHashToRGBA(hash);

  const outputImg = sharp(rgba, {
    raw: {
      width: rgbaWidth,
      height: rgbaHeight,
      channels: info.channels,
    },
  }).resize({
    width: OUTPUT_WIDTH,
    height: OUTPUT_HEIGHT,
    fit: 'fill',
    kernel: 'lanczos3',
  });

  return outputImg;
};

const main = async (): Promise<void> => {
  console.log(`Concurrency: ${sharp.concurrency()}`);

  ensureOutputFolder(OUTPUT_FOLDER);

  for await (const inputPath of globStream(INPUT_PATTERN, { withFileTypes: false })) {
    for (const fitType of FIT_TYPES) {
      const outputImg = await generateGradient(inputPath, fitType);

      const outputPath = `${OUTPUT_FOLDER}/${fitType}_${path.parse(inputPath).name}.png`;
      await outputImg.toFile(outputPath);

      console.log(`${inputPath} → ${outputPath} ✔`);
    }
  }

  console.log('Done!');
};

main();
