# thumbhash-gradients

## Development

Install [fnm](https://github.com/Schniz/fnm)

```bash
fnm install && fnm use && node --version
```

or

```bash
fnm use && fnm --version
```

```bash
npm install
```

```bash
npm run dev
```

## References

- https://sharp.pixelplumbing.com/
- https://github.com/aheckmann/gm
- https://github.com/jimp-dev/jimp + https://github.com/jimp-dev/jimp/tree/main/packages/jimp
- https://stackoverflow.com/questions/53434071/is-it-possible-to-add-noise-to-the-picture-using-jimp-in-node-js
- https://evanw.github.io/thumbhash/
- https://github.com/Schniz/fnm + https://nodejs.org/en
- https://github.com/sindresorhus/globby + https://www.npmjs.com/package/glob

## Notes

- `npm install sharp thumbhash glob fs-extra && npm install -D typescript ts-node @tsconfig/node18 @types/fs-extra`
- `rm -rf output/`
- If there is a problem running a script with [sharp](https://sharp.pixelplumbing.com/): `rm -rf node_modules/` + `npm install`
- `system_profiler SPDisplaysDataType | grep Resolution` ([source](https://superuser.com/a/447298)): `Resolution: 2560 x 1600 Retina`
- Option-Command-H and Command-M ([source](https://support.apple.com/en-us/HT201236))
